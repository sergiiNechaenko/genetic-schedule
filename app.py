from flask import Flask, escape, request, Response
from flask_cors import CORS
from gtts import gTTS
import json
import random
# get this object

import prettytable 

from schedule import Schedule
from genetic import GeneticOptimize

app = Flask(__name__)
CORS(app)

@app.route('/api/create_schedule',methods=['POST'])
def create_schedule():
	groups_unique = {}
	schedules = []
	for lecture in request.json['params']['lectures']:
		schedules.append(Schedule(lecture[1], lecture[0], lecture[2]))
		groups_unique[lecture[0]] = lecture[0]

	# optimization
	ga = GeneticOptimize(popsize=100, elite=10, maxiter=200)
	res = ga.evolution(schedules, 10)

	total_result = {}

	for group in groups_unique:
		# visualization
		vis_res = []
		for r in res:
		    if r.classId == group:
		        vis_res.append(r)

		result = [[i + 1, '', '', '', '', ''] for i in range(5)]
		for s in vis_res:
			weekDay = s.weekDay
			slot = s.slot
			text = '{}. {}. каб. 10{}'.format(s.courseId, s.teacherId, s.roomId)
			result[weekDay - 1][slot] = text

		total_result[group] = result

	return Response(json.dumps(total_result),  mimetype='application/json')